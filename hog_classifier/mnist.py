import numpy as np
from sklearn import datasets, svm
from matplotlib import pyplot

digits = datasets.load_digits()


clf = svm.SVC(gamma=0.001, C=100)
x,y  = digits.data[:-1], digits.target[:-1]

clf.fit(x,y)

print("prediction: ",clf.predict(digits.data[-1]))

plt.immshow(digits.images[-1], cmap=plt.cm.gray_r, interpolation="nearest")
plt.show()