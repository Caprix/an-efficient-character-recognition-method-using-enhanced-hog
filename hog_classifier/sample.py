from skimage import filters
from matplotlib import pyplot as plt
from skimage.io import imread, imshow
from skimage.transform import rescale, resize, downscale_local_mean

image_color = imread('English/Img/GoodImg/Bmp/Sample030/img030-00063.png', as_gray=True)
image_resized = resize(image_color, (64, 64), anti_aliasing=True)