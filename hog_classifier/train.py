import preprocessing
import argparse
import numpy as np # linear algebra
import json
from matplotlib import pyplot as plt
from matplotlib.image import imread
from skimage import color
from skimage.feature import hog, canny
from skimage.io import imread, imshow
from sklearn import svm
from sklearn.metrics import classification_report,accuracy_score
from PIL import Image
from skimage.transform import rescale, resize, downscale_local_mean
from sklearn.preprocessing import normalize

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

# from subprocess import check_output
# print(check_output(["ls", "../hog_classifier"]).decode("utf8"))


def train(model_name, training_dataset, validation_dataset):
    size = 64, 64
    train_steps = int(1e5)
    batch_size = 10
    crop_offset = 10

    # loading image locations
    train_dataset_locations = open(training_dataset, 'r').readlines()

    # loading image binaries
    img_binaries = list(map(lambda f:imread(f.strip()).astype('uint8'), train_dataset_locations))

    # resizing image
    img_binaries_resized = list(map(lambda f:resize(f, size, anti_aliasing=True), img_binaries))

    # cropping image
    img_cropped = list(map(lambda f:f[crop_offset:(f.shape[0]-crop_offset),crop_offset:(f.shape[1]-crop_offset)],   img_binaries_resized))

    # normalizing image
    img_normalized = list(map(lambda f:f / np.linalg.norm(f),   img_cropped))

    # edge detection
    # img_edges = list(map(lambda f: canny(f, sigma=3),   img_normalized))
    

    

#     train_dataset_binary = list(map(lambda f:np.array(Image.open(f.strip()).resize(size, Image.ANTIALIAS)).astype('uint8'), train_dataset_locations))
#     validation_dataset_binary = list(map(lambda f:np.array(Image.open(f.strip()).resize(size, Image.ANTIALIAS)).astype('uint8'),validation_dataset_locations))
#     imread('images.jpeg', as_gray=True)
#     print(dataset.shape)
#     print(train_dataset_binary[0])
#     print(train_dataset_binary[0].shape)
#     print(train_dataset_locations[0])
#     print(preprocessing.get_class(train_dataset_locations[0]))





    # img_length = 80
    #  reshaping dataset binary
#     print(train_dataset_binary)
#     print(train_dataset_binary.shape)
#     print(train_dataset_binary[0].shape)
#     data = [i.reshape(-1,3,img_h,img_w).transpose([0,2,3,1]) for i in train_dataset_binary]

#     plt.imshow(train_dataset_binary[51])
#     plt.show()
    
#     data_gray = [ color.rgb2gray(i) for i in train_dataset_binary]
#     plt.imshow(data_gray[51])
#     plt.show()
    

    # calculating the HOG values
    ppc = 16
    hog_images = []
    hog_features = []
    for image in img_normalized:
        fd,hog_image = hog(image, orientations=8, pixels_per_cell=(ppc,ppc),cells_per_block=(1, 1),block_norm= 'L2',visualize=True)
        hog_images.append(hog_image)
        hog_features.append(fd)

#     plt.imshow(hog_images[51])
#     plt.show()
    labels =  np.array(list(map(preprocessing.get_class,train_dataset_locations)))
    
    print("hello labels",labels)
    print(len(img_binaries_resized),len(labels))

    # training
    clf = svm.SVC(C=5, gamma=0.05)
    hog_features = np.array(hog_features)
    data_frame = np.stack(np.hstack((hog_features,labels)))
    np.random.shuffle(data_frame)

    percentage = 80
    partition = int(len(hog_features)*percentage/100)
#     print(data_frame)
#     print(data_frame.shape)
#     for i in img_binaries:
#         if i.shape != img_binaries[0].shape:
#             print("invalid shape",i.shape,img_binaries[0].shape)
    x_train, x_test = data_frame[:partition,:-1],  data_frame[partition:,:-1]
    y_train, y_test = data_frame[:partition,-1:].ravel() , data_frame[partition:,-1:].ravel()

    clf.fit(x_train,y_train)


    # predicting
    y_pred = clf.predict(x_test)

    print("Accuracy: "+str(accuracy_score(y_test, y_pred)))
    print('\n')
    print(classification_report(y_test, y_pred))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '-t', type=str, required=True, help='Training dataset name')
    parser.add_argument(
            '-v', type=str, required=False, help='Validation dataset name')
    parser.add_argument('-m', type=str, required=False, help='Model name')

    opt = parser.parse_args()
    train(opt.m, opt.t, opt.v)