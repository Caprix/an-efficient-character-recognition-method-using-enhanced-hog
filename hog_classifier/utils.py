def crop_image(img,cropx,cropy):
    print(img.shape)
    x = img.shape[0]
    y = img.shape[1]
    startx = x//2-(cropx//2)
    starty = y//2-(cropy//2)    
    return img[starty:starty+cropy,startx:startx+cropx]