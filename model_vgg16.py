import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Activation
from keras.layers.core import Dense, Flatten
from keras.optimizers import Adam
from keras.metrics import categorical_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import *

from utils import plot_confusion_matrix

vgg16_model = keras.applications.vgg16.VGG16()
# vgg16_model.summary()

model = Sequential()
for layer in vgg16_model.layers[:-1]:
    model.add(layer)

for layer in model.layers:
    layer.trainable = False

model.add(Dense(2, activation='softmax'))
# model.summary()

# image paths
train_path = 'images/train'
valid_path = 'images/valid'
test_path = 'images/test'

# generating images from paths
train_batches = ImageDataGenerator().flow_from_directory(train_path, target_size=(224,224), classes=['spam','ham'], batch_size=20)
valid_batches =  ImageDataGenerator().flow_from_directory(valid_path, target_size=(224,224), classes=['spam','ham'], batch_size=20)
test_batches = ImageDataGenerator().flow_from_directory(test_path, target_size=(224,224), classes=['spam','ham'], batch_size=10)

# compiling and training model with generated images
model.compile(Adam(lr=.0001), loss='categorical_crossentropy', metrics=['accuracy'])
model.fit_generator(train_batches, steps_per_epoch=75, validation_data=valid_batches, validation_steps=8, epochs=5, verbose=2)


# making prediction on unseen data and generate confusion matrics
test_imgs, test_labels = next(test_batches)
test_labels = test_labels[:,0]

prediction = model.predict_generator(test_batches,steps=1,verbose=0)
print(test_labels)
print(prediction)

cm = confusion_matrix(test_labels,np.round(prediction[:,0]))

# code to plot confution confusion_matrix
cm_plot_labels = ["spam","ham"]
plot_confutsion_matrix(cm, cm_plot_labels, title="Confusion Matrix")