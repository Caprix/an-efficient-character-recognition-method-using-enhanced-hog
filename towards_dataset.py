import pandas as pd 
import numpy as np 
import cufflinks as cf
cf.go_offline()
cf.set_config_file(offline=False, world_readable=True)

df = pd.read_csv('consumer_complaints.csv', low_memory=False)
df.info()
print(df['product'].value_counts())


# df.loc[df['product'] == 'Credit reporting', 'Product'] = 'Credit reporting, credit repair services, or other personal consumer reports'
# df.loc[df['product'] == 'Credit card', 'Product'] = 'Credit card or prepaid card'
# df.loc[df['product'] == 'Payday loan', 'Product'] = 'Payday loan, title loan, or personal loan'
# df.loc[df['product'] == 'Virtual currency', 'Product'] = 'Money transfer, virtual currency, or money service'
# df = df[df['product'] != 'Other financial service']

# df['product'].value_counts().sort_values(ascending=False).iplot(kind='bar', 
#                                                                     yTitle='Number of Complaints', 
#                                                                     title='Number complaints in each product')

# def print_plot(index):
#     example = df[df.index == index][['consumer_complaint_narrative', 'product']].values[0]
#     if len(example) > 0:
#         print(example[0])
#         print('product:', example[1])

# print_plot(100)